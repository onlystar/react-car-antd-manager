import React from 'react'
import ReactDOM from 'react-dom'
// import Admin from './admin'
import Router from './router'
// import Demo from './pages/route_demo/route1/Home'
// import Demo from './pages/route_demo/route2/router'
// import Demo from './pages/route_demo/route3/router'
import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(<Router/>, document.getElementById('root'))
registerServiceWorker()
